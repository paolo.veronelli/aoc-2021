{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module LoopLeft where

import Data.Machine (Mealy (Mealy), auto, runT, source, (~>))
import Protolude
  ( Bool
  , Either (..)
  , Eq ((==))
  , Identity (Identity)
  , Maybe (Just, Nothing)
  , fix
  , maybe
  , ($)
  , (&)
  )

loopLeft :: Mealy (Either b a) (Either b a)
loopLeft =
  Nothing & fix \go mb -> Mealy \case
    Right x -> (maybe (Right x) Left mb, go mb)
    Left y -> (Left y, go $ Just y)

test :: Bool
test =
  do runT $ source [Right 'a', Left (), Right 'c'] ~> auto loopLeft
    == do Identity [Right 'a', Left (), Left ()]
