{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Aoc9'3 where

import Control.Arrow ((&&&))
import Data.List (zip3)
import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as M
import Data.Semigroup (Min (Min), getMin)
import qualified Data.Set as Set
import qualified Data.Text as T
import Protolude hiding (State)
import Protolude.Partial (read)
import Protolude.Unsafe (unsafeHead)
import Text.Pretty.Simple (pPrint)

main :: IO ()
main = do
  xs <- lines <$> readFile "9-4096-4.in-1"
  pPrint $
    do getSum . product
      . fmap fst
      . take 3
      . sortOn Down
      &&& do sum . fmap (succ . read @Int . pure . getMin . snd)
      $ M.elems $
        collapse $ harvest
          do '9'
          do \x -> (Sum 1, Min x)
          do T.length $ unsafeHead xs
          do concatMap T.unpack xs

newtype Id = Id Int deriving (Eq, Ord, Show, Num)

type Values r = MonoidalMap Id r

type Subs = MonoidalMap Id (Set Id)

newtype Harvest r = Harvest (Values r, Subs) deriving (Semigroup, Monoid, Show)

harvest :: (Semigroup r, Eq a, Show a) => a -> (a -> r) -> Int -> [a] -> Harvest r
harvest k z l xs = snd $ fix \(~(choices, _)) ->
  second mconcat $
    unzip $
      snd $ mapAccumL
        do contrib k z
        do Id 0
        do zip3 xs (fixZeros l $ Nothing : choices) (replicate l Nothing <> choices)

fixZeros :: Int -> [Maybe Id] -> [Maybe Id]
fixZeros l = zipWith
  ($)
  do concat $ repeat $ const Nothing : replicate (l - 1) identity

mkHarvest :: Id -> (t -> r) -> t -> Subs -> Harvest r
mkHarvest id z x e = Harvest (M.singleton id $ z x, e)

contrib :: (Eq a, Semigroup r) => a -> (a -> r) -> Id -> (a, Maybe Id, Maybe Id) -> (Id, (Maybe Id, Harvest r))
contrib k _ id ((== k) -> True, _, _) = (id, (Nothing, mempty))
contrib _ z id (x, Nothing, Nothing) = (id + 1, (Just id, mkHarvest id z x mempty))
contrib _ z id (x, Just g, Nothing) = (id, (Just g, mkHarvest g z x mempty))
contrib _ z id (x, Nothing, Just g) = (id, (Just g, mkHarvest g z x mempty))
contrib _ z id (x, Just g', Just g)
  | g == g' = (id, (Just g, mkHarvest g z x mempty))
  | otherwise = (id, (Just g, mkHarvest g z x $ M.singleton (max g g') $ Set.singleton $ min g g'))

collapse :: Semigroup r => Harvest r -> Values r
collapse (Harvest (vs, ss)) = case M.lookupMax ss of
  Nothing -> vs
  Just (id, sids) -> case Set.maxView sids of
    Nothing -> collapse $ Harvest (vs, M.delete id ss)
    Just (id', sids') -> collapse $
      Harvest $ (,)
        do
          case M.lookup id vs of
            Nothing -> panic "flowers"
            Just x -> M.singleton id' x <> M.delete id vs
        do
          M.singleton id' sids' <> M.delete id ss
