{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc4 where

import qualified Control.Foldl as F
import Control.Lens (Ixed (ix), (^.))
import qualified Data.List.Split as List
import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as M
import Data.String (String)
import qualified Data.String as String
import Protolude hiding (Map, State)
import Protolude.Partial (read)
import Streaming
  ( Compose (Compose)
  , MFunctor (hoist)
  , Of ((:>))
  , Stream
  , concats
  , distribute
  , zips
  )
import qualified Streaming.Prelude as S
import qualified Data.List.NonEmpty as NE

type Map = MonoidalMap 
-- untagged counts for column or row
data V = C Int | R Int deriving (Show, Eq, Ord)

-- elements of the boards
newtype K = K Int deriving (Show, Eq, Ord)

-- board index
newtype N = N Int deriving (Show, Eq, Ord, Enum, Num)

-- element index
data M = M N V deriving (Show, Eq, Ord)

-- from entry to all their indexes
type Remove = Map K (Map M (Sum Int))

-- countdown for any line
type Count = Map M (Sum Int)

-- untagged pieces, it comes out the weight is not needed as entries are unique
type Board = Map N (Map K (Sum Int))

-- Remove is not really a state
data State = State Remove Count Board deriving (Show)

mkState :: Monad m => Stream (Of (N, Int, Int, K)) m () -> m State
mkState = S.fold_
  do
    \(State r d b) (n, l, c, k) ->
      let vs = M.fromList $ zip [M n (R l), M n (C c)] $ repeat (Sum 1)
       in State
            do r <> do M.singleton k vs
            do d <> vs
            do b <> do M.singleton n $ M.singleton k 1
  do State mempty mempty mempty
  do identity

-- a bit borked as the N is going to be always even because of breakBoards empty groups, who cares, they are unique
unfoldBoard :: Monad m => Compose (Of N) (Stream (Of String) m) b -> Stream (Of (N, Int, Int, K)) m b
unfoldBoard (Compose (board :> x)) = do
  S.for
    do S.scanned (const . succ) (- 1) identity x
    do
      \(s, l) -> S.each $ do
          (c, k) <- zip [0 ..] $ unique $ K . read <$> String.words s
          pure (board, l, c, k)

unique :: (Foldable f, Eq b) => f b -> [b]
unique = fmap NE.head . NE.group

breakBoards :: Monad m => Stream (Of String) m () -> Stream (Compose (Of N) (Stream (Of String) m)) m ()
breakBoards = zips (S.each [0 ..]) . S.groupBy do \x y -> not $ null x || null y

parseState :: MonadIO m => Stream (Of String) m () -> m State
parseState =
  mkState
    . concats
    . do S.maps unfoldBoard
    . breakBoards

main :: IO ()
main = do
  (sks, s) <- S.readFile "aoc4-r.txt" $ \s0 -> do
    ms <- S.next s0
    case ms of
      Left _ -> panic "empty stream"
      Right (k, s) -> (k,) <$> parseState (S.drop 1 s)
  let ks = K . read <$> List.splitOn "," sks
  (f, l) <- F.purely S.fold_ ((,) <$> F.head <*> F.last) $ removerS s $ S.each ks
  print f
  print l

-- logicT or ListT or whatever could just fit , but why not Streaming
removerS :: (Monad m) => State -> Stream (Of K) m () -> Stream (Of Int) m ()
removerS s xs = flip evalStateT s . distribute $ removeK $ hoist lift xs

removeK :: MonadState State m => Stream (Of K) m () -> Stream (Of Int) m ()
removeK s = do
  State r _counts _boards <- get
  removeM $ S.for
    do s
    do \k -> S.map (k,) $ S.each (M.assocs $ r ^. ix k) -- would be nice to filter by gone board (cost not clear)

removeM :: MonadState State m => Stream (Of (K, (M, Sum Int))) m () -> Stream (Of Int) m ()
removeM = flip S.for $ \(k@(K ki), (l@(M n _), Sum q)) -> do
  State r c b <- get
  let count = getSum (c M.! l) - q
      b' = M.adjust (M.delete k) n b
  b'' <-
    if count == 0
      then do
        case M.lookup n b' of
          Just xb -> do
            S.yield $ (ki *) $ sumBoard xb
            pure $ M.delete n b' -- remove the board
          Nothing -> pure b' -- board was already gone
      else pure b' -- not 0 
  put $ State
    do r
    do M.insert l (Sum count) c
    do b''

sumBoard :: Map K (Sum Int) -> Int
sumBoard = sum . fmap (\(K k, Sum r) -> k * r) . M.assocs
