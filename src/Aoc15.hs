{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc15 where

import Control.Monad.State.Strict
import Control.Monad.Trans.Maybe (MaybeT (MaybeT), runMaybeT)
import Data.Array (Array, Ix (inRange), array, bounds, (!))
import Data.Functor.Of (Of ((:>)))
import Data.PQueue.Min (MinQueue)
import qualified Data.PQueue.Min as PQ
import qualified Data.Set as Set
import Protolude hiding (evalState)
import Protolude.Partial ((!!))
import Protolude.Unsafe (unsafeHead, unsafeRead)
import qualified Streaming.Prelude as S

type P = (Int , Int)

type Cost = Array P Int

data Z = Z Int P deriving (Eq, Ord)

data Front = Front
  { front :: MinQueue Z
  , visited :: Set P
  }

{-# INLINE at #-}
at :: (Ix i) => Array i p -> i -> Maybe p
at a p
  | inRange (bounds a) p = Just $ a ! p
  | otherwise = Nothing

{-# INLINE neighbors #-}
neighbors :: Cost -> P -> Int -> [(P, Int)]
neighbors c (x, y) v = mapMaybe
  do \p -> at c p <&> \s -> (p, v + s)
  do [(x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)]

best :: (MonadState Front m) => MaybeT m Z
best = do
  Front m w <- get
  (z, ps) <- MaybeT . pure $ PQ.minView m
  put $ Front ps w 
  pure z

{-# INLINE update #-}
update :: (MonadState Front m) => P -> Int -> m ()
update p v = modify $ \f@(Front m w) ->
  if  p `Set.member` w
    then f
    else Front
      do PQ.insert (Z v p) m
      do Set.insert p w

{-# INLINE step #-}
step :: (MonadState Front m) => Cost -> MaybeT m Z
step c = do
  r@(Z v p) <- best
  lift $ forM_ (neighbors c p v) $ uncurry update
  pure r

solve :: Cost -> P -> P -> Maybe Int
solve c s e = flip evalState s0 $ runMaybeT go
  where
    go = do
      Z v r <- step c
      if r == e then pure v else go
    s0 = Front
      do PQ.singleton $ Z (c ! s) s
      do Set.singleton s

main :: IO ()
main = do
  (xs :: [[Int]]) :> nr <- S.readFile "aoc15.txt" $ \l ->
    l & do S.map $ fmap $ unsafeRead . pure
      & S.store S.length_
      & do S.toList
  let nc = length $ unsafeHead xs
      b = (fromIntegral $ nc * 5 - 1, fromIntegral $ nr * 5 -1)
      a = array ((0, 0), b) $ tiled nc nr xs 5
  print $ subtract (a ! (0, 0)) <$> solve a (0, 0) (fromIntegral $ nc - 1, fromIntegral $ nr - 1)
  print $ subtract (a ! (0, 0)) <$> solve a (0, 0) b

tiled :: Int -> Int  -> [[Int]] -> Int -> [(P, Int)]
tiled nc nr xss n = do
  i <- [0 .. n - 1]
  j <- [0 .. n - 1]
  let d = i + j
  (l, xs) <- zip [0 ..] xss
  (c, x) <- zip [0 ..] xs
  pure
    ( (fromIntegral $ c + j * nc, fromIntegral $ l + i * nr)
    , values x !! d
    )

values :: Int -> [Int]
values = iterate $ \x ->
  let x' = succ x
   in if x' > 9 then 1 else x'
