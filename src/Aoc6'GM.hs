{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE BlockArguments #-}

-- giorgio mossa strictified
module Aoc6'GM where

import Data.Function ((&))
import Data.List (foldl')
import qualified Data.Map.Strict as M
import System.IO
import Prelude hiding (init)

-- State represent the fishes' state
type State = M.Map Int Integer

-- a state :: State is such that
-- lookup i state represent the number of fishes
-- that will reproduce in i-days.

start :: State
start = M.fromAscList [(i, 0) | i <- [0 .. 8]]

readInput :: [Int] -> State
readInput = ausil start
  where
    ausil acc [] = acc
    ausil !acc (fish : fishes) =
      ausil (M.update (\x -> Just (x + 1)) fish acc) fishes

-- unsafe extraction of values from State
{-# INLINEABLE update #-}
update :: State -> State
update state =
  M.empty
    & M.insert 0 (state M.! 1)
    & M.insert 1 (state M.! 2)
    & M.insert 2 (state M.! 3)
    & M.insert 3 (state M.! 4)
    & M.insert 4 (state M.! 5)
    & M.insert 5 (state M.! 6)
    & M.insert 6 (state M.! 7 + state M.! 0)
    & M.insert 7 (state M.! 8)
    & M.insert 8 (state M.! 0)

iter :: Int -> (a -> a) -> a -> a
iter 0 _ !init = init
iter n f !init = iter (n -1) f (f init)

summer :: State -> Integer
summer state = foldl' (\acc index -> acc + state M.! index) 0 [0 .. 8]

main :: IO ()
main = do
  raw <- readFile "aoc6.txt"
  let list = read ('[' : raw ++ "]") :: [Int]
      init = readInput list
      val = iter 80 update init
      val2 = iter 256 update init
  print $ "After 80 days there are " ++ show (summer val) ++ " fishes"
  print $ "After 100000 days there are " ++ show (summer val2) ++ " fishes"
