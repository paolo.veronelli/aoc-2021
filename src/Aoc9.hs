{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Aoc9 where

import Control.Lens (ifoldMap)
import Data.Array ( Ix(inRange), Array, listArray, bounds, (!) )
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import ListT (fromFoldable)
import qualified ListT as T
import Protolude
import Protolude.Unsafe ( unsafeHead )
import qualified Streaming.Prelude as S
import Prelude hiding (print, product, sum)
import Streaming ( Of((:>)) )

type P = (Int, Int)

type A = Array P Int

main :: IO ()
main = do
  xs :> ln <- S.readFile "9-4096-4.in-1" $ \l ->
    l & do S.map $ fmap $ read . pure 
      & S.store S.length_
      & do S.toList
  let ls = links a
      a = listArray ((1, 1), (length $ unsafeHead xs, ln)) $ concat $ transpose xs
      bs = bottoms ls
  print $ a ! (4096, 4096)
  print $ sum $ succ . (a !) <$> bs
  print $ product $ take 3 . sortOn Down . fmap length $ basin ls <$> bs

{-# inline  at #-}
at :: (Ix i, Num p) => Array i p -> i -> Maybe p
at a p
  | inRange (bounds a) p = Just $ a ! p
  | otherwise = Nothing


down :: A -> P -> Int -> B 
down _ _ 9 = mempty 
down a p@(x, y) c =
  let n = (x, y - 1)
      s = (x, y + 1)
      e = (x + 1, y)
      w = (x -1, y)
      ds = filter (maybe False (liftA2 (&&) (>= c) (< 9)) . at a) [n, s, e, w]
   in M.singleton p ds

type B = Map P [P]

links :: A -> B
links  = down >>= ifoldMap -- fnurglwiz

bottoms :: B -> [P]
bottoms b = toList $ M.keysSet b `Set.difference`  Set.fromList  (concat $ M.elems b)

basin :: B -> P -> Set P
basin b = flip execState mempty . T.toList . go 
  where
    go p = do
      done <- Set.insert p <$> get
      put done 
      p' <- fromFoldable $ M.findWithDefault mempty p b
      when
        do p' `Set.notMember` done
        do go p'
