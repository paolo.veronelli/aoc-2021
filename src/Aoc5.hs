{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction, StrictData #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc5 where

import Control.Lens
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString (parseOnly)
import Data.Attoparsec.ByteString.Char8 (Parser, decimal, string)
import qualified Data.Set as Set
import Protolude
import Streaming (Of (..), Stream)
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

readLine :: Monad m => Parser a -> Stream (SBS.ByteStream m) m r -> Stream (Of a) m r
readLine f x =
  x
    & S.mapped
      do fmap (first $ parseOnly f) . SBS.toStrict
    & do
      flip
        S.for
        do either (const $ pure ()) S.yield

type Command = Either Int Int

parseLine :: Parser Line
parseLine = do
  x1 <- decimal
  void $ string ","
  y1 <- decimal
  void $ string " -> "
  x2 <- decimal
  void $ string ","
  y2 <- decimal
  pure $ Line
    do Point x1 y1
    do Point x2 y2

data Point = Point Int Int deriving (Show, Eq, Ord)

data Line = Line Point Point deriving (Show)

enumerateD :: (Ord a, Enum a, Num a) => a -> a -> [a]
enumerateD a b
  | a <= b = [a .. b]
  | otherwise = [a, a -1 .. b]

points :: Bool -> Line -> [Point]
points t (Line (Point x1 y1) (Point x2 y2))
  | x1 == x2 = [Point x1 y | y <- enumerateD y1 y2]
  | y1 == y2 = [Point x y1 | x <- enumerateD x1 x2]
  | t = [Point x y | y <- enumerateD y1 y2 | x <- enumerateD x1 x2]
  | otherwise = []

main :: IO ()
main = runResourceT do
  ls <-
    SBS.readFile "aoc5.txt"
      & SBS.lines
      & readLine parseLine
      & do S.store $ counting True
      & counting False
  print ls

counting :: Monad m => Bool -> Stream (Of Line) m r -> m (Of Int r)
counting t x =
  x & do flip S.for $ S.each . points t
    & repetitions
    & S.filter snd
    & S.length

repetitions :: (Monad m, Ord k) => Stream (Of k) m r -> Stream (Of (k, Bool)) m r
repetitions = S.scanned
  do
    \(ones, mores, _) x ->
      if
          | Set.member x ones -> (Set.delete x ones, Set.insert x mores, True)
          | Set.member x mores -> (ones, mores, False)
          | otherwise -> (Set.insert x ones, mores, False)
  do (mempty, mempty, False)
  do view _3
