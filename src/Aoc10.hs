{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc10 where

import Control.Monad.Trans.Resource (runResourceT)
import qualified Data.ByteString.Char8 as B
import Data.List ((!!))
import Protolude
import Streaming (Of ((:>)))
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

opposite :: Char -> Maybe Char
opposite '(' = Just ')'
opposite '[' = Just ']'
opposite '{' = Just '}'
opposite '<' = Just '>'
opposite _ = Nothing

consume :: Char -> [Char] -> [Char] -> Either Char [Char]
consume x ys xs = case opposite x of
  Just y' -> parensOf (y' : ys) xs
  Nothing -> Left x

parensOf :: [Char] -> [Char] -> Either Char [Char]
parensOf r [] = Right r
parensOf (y : ys) (x : xs)
  | x == y = parensOf ys xs
  | otherwise = consume x (y : ys) xs
parensOf [] (x : xs) = consume x [] xs

value :: Char -> Int
value ')' = 3
value ']' = 57
value '}' = 1197
value '>' = 25137
value _ = panic "smoke"

rvalue :: Char -> Int
rvalue ')' = 1
rvalue ']' = 2
rvalue '}' = 3
rvalue '>' = 4
rvalue _ = panic "mirrors"

main :: IO ()
main = do
  ls :> l :> v1 :> () <-
    runResourceT $
      SBS.readFile "aoc10.txt"
        & SBS.lines
        & S.mapped
          do fmap (first $ parensOf [] . B.unpack) . SBS.toStrict
        & S.partitionEithers
        & S.map value
        & S.sum
        & do
          S.map $ foldl
            do \s c -> s * 5 + rvalue c
            do 0
        & S.store S.length
        & S.toList
  print v1
  print $ (!! div l 2) $ sort ls


