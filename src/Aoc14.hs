{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Aoc14 where

import Control.Arrow ((&&&))
import Control.Lens (lmap)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Array (Array, array, (!))
import Data.Attoparsec.ByteString.Char8 (Parser, anyChar, parseOnly, string)
import qualified Data.ByteString.Char8 as B
import Data.Map.Monoidal (MonoidalMap)
import qualified Data.Map.Monoidal as M
import qualified Data.Map.Strict as Ma
import Protolude
import Protolude.Unsafe (unsafeInit, unsafeTail)
import Streaming (Of ((:>)))
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

parseX
  :: Monad m
  => StateT s Parser a
  -> S.Stream (Of ByteString) (StateT s m) r
  -> S.Stream (Of a) (StateT s m) r
parseX l =
  S.concat . S.mapM do
    \s -> do
      t <- get
      either
        do pure . Left
        do \(x, t') -> put t' $> Right x
        do parseOnly (runStateT l t) s

data Mapping = Mapping Int (Map Char Int)

mapping :: MonadState Mapping m => Char -> m Int
mapping c = do
  Mapping n m <- get
  case Ma.lookup c m of
    Nothing -> do
      let i = succ n
      put $ Mapping i $ Ma.insert c n m
      pure n
    Just i -> pure i

type Rule = (Int, Int, Int)

charI :: StateT Mapping Parser Int
charI = lift anyChar >>= mapping

parseRule :: StateT Mapping Parser Rule
parseRule = do
  s <- charI
  e <- charI
  lift $ void $ string " -> "
  i <- charI
  pure (s, e, i)

type Cache = Array (Int, Int, Int) Scores

type Scores = MonoidalMap Int (Sum Integer)

runRule :: Int -> Rule -> Cache -> Scores
runRule k (s, e, i) m = M.adjust (subtract 1) i $ m ! (s, i, k - 1) <> m ! (i, e, k - 1)

cache :: [Rule] -> Int -> Int -> Cache
cache rs d l = fix \m ->
  array ((0, 0, 0), (d, d, l)) do
    (s, e, i) <- rs
    (:)
      do (,) (s, e, 0) $ mconcat [M.singleton s $ Sum 1, M.singleton e $ Sum 1]
      do
        j <- [1 .. l]
        pure $ (,) (s, e, j) $ runRule j (s, e, i) m

query :: Int -> ((Int, Int, Int) -> Scores) -> [Int] -> Scores
query l f xs = foldl'
  do flip $ M.adjust (subtract 1)
  do foldMap (lmap (\(x, y) -> (x, y, l)) f) $ zip <*> unsafeTail $ xs
  do unsafeInit $ unsafeTail xs

main :: IO ()
main = do
  ((Just points, folds), Mapping t m) <-
    runResourceT $
      flip runStateT (Mapping 0 mempty) $
        SBS.readFile "aoc14.txt"
          & SBS.lines
          & S.mapped SBS.toStrict
          & do
            \s -> do
              ps :> sfs <-
                s
                  & S.break (== mempty)
                  & S.head
              fs <- S.toList_ $ parseX parseRule sfs
              pure (ps, fs)
  let q n = getSum $
        uncurry subtract $
          (minimum &&& maximum) $ query
            do n
            do (cache folds t 1000 !)
            do (m Ma.!) <$> B.unpack points
  print $ q 10
  print $ q 1000
