{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc6 where

import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Protolude
import Protolude.Partial (read)
import Protolude.Unsafe (unsafeHead)
import Streaming hiding (Sum)
import qualified Streaming.Prelude as S

type Gestation = Int

type Population = MonoidalMap Gestation (Sum Int)

rules :: Gestation -> [Gestation]
rules 0 = [6, 8]
rules n = [n - 1]

step :: Population -> Population
step p = mconcat $ do
  (g, n) <- M.assocs p
  g' <- rules g
  pure $ M.singleton g' n

main :: IO ()
main = do
  gs :: [Int] <-
    fmap (read . T.unpack)
      . T.splitOn ","
      . unsafeHead
      . T.lines
      <$> T.readFile do "aoc6.txt"

  let (Just p2 :> Just p1 :> ()) =
        runIdentity $
          S.iterate step do mconcat [M.singleton g $ Sum 1 | g <- gs]
            & do S.take 257
            & S.copy
            & do S.drop 80
            & S.head
            & S.last

  print $ getSum $ sum p1
  print $ getSum $ sum p2

-- print $ sum p2
