{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Median where

import Control.Comonad
import Control.Foldl (Fold (Fold))
import qualified Control.Foldl as F
import Data.FingerTree
  ( FingerTree
  , Measured (..)
  , ViewL ((:<))
  , ViewR ((:>))
  , split
  , viewl
  , viewr
  , (|>)
  )
import Data.Semigroup (Max (Max))
import Protolude

newtype V a = V a

instance (Ord a, Bounded a) => Measured (Sum Int, Max a) (V a) where
  measure (V x) = (Sum 1, Max x)

data Median a = Median
  { median_ft :: FingerTree (Sum Int, Max a) (V a)
  , median_size :: Int
  }

insert :: (Ord a, Bounded a) => a -> Median a -> Median a
insert x (Median s l) =
  let (preds, sucs) = split (\(Sum _, Max y) -> y >= x) s
   in Median
        do (preds |> V x) <> sucs
        do succ l

median :: (Ord a, Bounded a) => Median a -> [a]
median (Median _ 0) = []
median (Median s l@(odd -> True)) =
  let (viewr -> _ :> V m, _) =
        split (\(Sum i, Max _) -> i > (l + 1) `div` 2) s
   in [m]
median (Median s l) =
  let (viewr -> _ :> V x, viewl -> V y :< _) =
        split (\(Sum i, Max _) -> i > l `div` 2) s
   in [x, y]

type MedianF a b = Fold a b

medianF :: (Ord a, Bounded a) => ([a] -> b) -> MedianF a b
medianF f = Fold
  do flip insert
  do Median mempty 0
  do f . median

addValues :: Foldable t => t a -> MedianF a b -> MedianF a b
addValues xs = extend (`F.fold` xs)

medianAccum :: [[Int]] -> [[Int]]
medianAccum =
  snd <$> mapAccumL
    do
      \s x ->
        let s' = addValues x s
         in (s', extract s')
    do medianF @Int identity

testMedianAccums :: Bool
testMedianAccums =
  medianAccum
    [ []
    , [1, 2, 3]
    , [10, 11, 12]
    , [20]
    ]
    == [ []
       , [2]
       , [3, 10]
       , [10]
       ]
