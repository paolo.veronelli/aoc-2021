{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Aoc9'2 where

import Control.Lens (ifoldl')
import Data.Array (Array, Ix (inRange), bounds, listArray, (!))
import Data.List (partition)
import qualified Data.Set as Set
import Protolude hiding (State)
import Protolude.Unsafe (unsafeHead)
import Streaming (Of ((:>)))
import qualified Streaming.Prelude as S
import Prelude hiding (print, product, sum)

type P = (Int, Int)

type A = Array P Int

main :: IO ()
main = do
  xs :> ln <- S.readFile "9-4096-4.in-1" $ \l ->
    l & do S.map $ fmap $ read . pure
      & S.store S.length_
      & do S.toList
  let a = listArray ((1, 1), (ln, length $ unsafeHead xs)) $ concat  xs

  print $ product $ take 3 . sortOn Down . fmap length $ scanA a

scanA :: A -> [Set P]
scanA a = filter (not . Set.null) $ (\(State o t) -> o <> t) $ ifoldl' (down a) (State [] []) a

down :: A -> P -> State -> Int -> State
down a p@(x,y) s@(State o t) i = let 
  s'@(State o' t') = case y of 
    1 -> State (o <> o'')   t'' 
    _ -> s
  (o'', t'') = partition 
    do all (< x) . fmap fst . Set.elems 
    do t
  in case i of 
      9 -> s' 
      _ -> State o' $ merge (maskL a p) t'

{-# INLINE at #-}
at :: (Ix i) => Array i p -> i -> Maybe p
at a p
  | inRange (bounds a) p = Just $ a ! p
  | otherwise = Nothing

type Id = Int 
data State = State [Set P]  [Set P]

merge :: Set P ->  [Set P] -> [Set P] 
merge p s =
  let (q, m) = partition (Set.null . Set.intersection p) s
   in  (: q) $ case m of 
     [] -> p
     ms -> Set.unions $ p : ms 

maskL :: A -> P -> Set P
maskL a p@(x, y) =
  Set.fromList $ (p:) $  
    fmap fst $
      filter ((< 9) . snd) $
        mapMaybe (\i -> (i,) <$> at a i) [s, e]
  where
    s = (x, y + 1)
    e = (x + 1, y)

-- type B = Map P [P]

-- links :: A -> B
-- links a = ifoldMap (down a) a

-- bottoms :: B -> [P]
-- bottoms b = toList $ M.keysSet b `Set.difference`  Set.fromList  (concat $ M.elems b)

-- basin :: B -> P -> Set P
-- basin b = flip execState mempty . T.toList . go
--   where
--     go p = do
--       done <- Set.insert p <$> get
--       put done
--       p' <- fromFoldable $ M.findWithDefault mempty p b
--       when
--         do p' `Set.notMember` done
--         do go p'
