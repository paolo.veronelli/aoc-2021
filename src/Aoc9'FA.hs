{-# LANGUAGE BlockArguments #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}
{-# OPTIONS_GHC -Wno-unused-local-binds #-}

module Aoc9'FA where

import qualified Data.Array as A -- da `array`
import qualified Data.List as L

type Coords = (Int, Int)

input :: IO (A.Array Coords Int)
input = do
  c <- lines <$> readFile "aoc9.txt"
  let w = length . head $ c
      h = length c
  return $ A.listArray ((1, 1), (h, w)) (map (read . (: [])) $ concat c)

adjs :: A.Array Coords Int -> Coords -> [(Coords, Int)]
adjs m (r, c) =
  let (w, h) = A.bounds m
      css = filter inside [(r, c + 1), (r, c -1), (r + 1, c), (r -1, c)]
   in map (\cs -> (cs, m A.! cs)) css
  where
    inside (wr, wc) =
      let (mh, mw) = snd (A.bounds m)
       in wr > 0 && wc > 0 && wr <= mh && wc <= mw

uppers :: A.Array Coords Int -> (Int, Int) -> [(Coords, Int)]
uppers m cs = filter ((> m A.! cs) . snd) (adjs m cs)

lowests :: A.Array Coords Int -> [(Coords, Int)]
lowests m = filter (\(cs, _) -> uppers m cs == adjs m cs) (A.assocs m)

-- 15 / 530
sol1 :: A.Array Coords Int -> Int
sol1 = sum . map ((+ 1) . snd) . lowests

sink :: A.Array Coords Int -> Coords -> [(Coords, Int)]
sink m cs =
  let r = m A.! cs
      us = filter ((/= 9) . snd) (uppers m cs)
   in L.nub . L.sort $ us ++ concatMap (sink m . fst) us

-- 1134 / 1019494
sol2 :: A.Array Coords Int -> Int
sol2 m =
  let ls = map fst (lowests m)
      ss = map ((+ 1) . length . sink m) ls
   in product . take 3 . reverse $ L.sort ss

main :: IO ()
main = do
  input >>= print . sol1
  input >>= print . sol2
