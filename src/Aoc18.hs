{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeApplications #-}

module Aoc18 where

import Control.Arrow ((&&&), (***))
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString (Parser, parseOnly)
import Data.Attoparsec.ByteString.Char8
  ( char
  , decimal
  )
import qualified Data.List as List
import Protolude hiding (evalStateT, StateT, evalState, magnitude, State, get , put)
import Protolude.Unsafe (unsafeLast)
import qualified Streaming.ByteString as SBS
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S
import Control.Monad.Trans.Maybe (MaybeT (runMaybeT))
import Control.Monad.State.Strict
import Data.String (String)

data Euler = E | L Int Euler | D Euler | U Euler deriving (Show)

data Tree = Tree Tree Tree | Label Int deriving (Show)

parseTree :: Parser Tree
parseTree =
  Label <$> decimal
    <|> do
      Tree
        <$> do
          void $ char '['
          parseTree
        <*> do
          void $ char ','
          parseTree <* char ']'

mkEuler :: Tree -> Euler
mkEuler = go E
  where
    go f (Label n) = L n f
    go f (Tree t1 t2) = D $ go (go (U f) t2) t1


splits :: Euler -> Maybe Euler
splits (L x f)
  | x > 9 =
    let (v, r) = x `divMod` 2
     in Just $ D $ L v $ L (v + r) $ U f
  | otherwise = L x <$> splits f
splits (D e) = D <$> splits e
splits (U e) = U <$> splits e
splits E = Nothing

explodeHead :: (Int -> (Bool, Euler -> Euler)) -> Int -> Euler -> Maybe Euler
explodeHead _ _ E = Nothing
explodeHead p ((>= 4) -> True) (D (L x (L y (U f)))) = Just $ case p x of
  (t, z) -> case explodeRest True y f of
    (t', r) -> z $ if t /= t' then L 0 r else r
explodeHead p n (D f) = explodeHead (reset False D p) (n + 1) f
explodeHead p n (U f) = explodeHead (reset False U p) (n - 1) f
explodeHead p n (L y f) = explodeHead (\x -> reset True (L (x + y)) p 0) n f

reset
  :: Bool
  -> (Euler -> Euler)
  -> (Int -> (Bool, Euler -> Euler))
  -> Int
  -> (Bool, Euler -> Euler)
reset t k p x = (const t *** (. k)) $ p x

explode :: Euler -> Maybe Euler
explode = explodeHead (const (False, identity)) 0

explodeRest :: Bool -> Int -> Euler -> (Bool, Euler)
explodeRest _ _ E = (False, E)
explodeRest r y (D (L x f)) = (r, D (L (x + y) f))
explodeRest r y (L x f) = (r, L (x + y) f)
explodeRest _ y (D f) = D <$> explodeRest False y f
explodeRest _ y (U f) = U <$> explodeRest False y f

rules :: Euler -> Maybe Euler
rules e = explode e <|> splits e

down :: (Euler -> c) -> Euler -> Euler -> c
down f E = f
down f (L x g) = down (f . L x) g
down f (D g) = down (f . D) g
down f (U g) = down (f . U) g

add :: Euler -> Euler -> Euler
add x y = D $ down identity x $ down identity y $ U E

couples :: [t] -> [(t, t)]
couples [] = []
couples (x : xs) =
  do (x,) <$> xs
    <> do (,x) <$> xs
    <> do couples xs

wtf :: Euler -> Int
wtf x = let
  Just r = mkTree x
  in magnitude r


type MkTree  = StateT Euler Maybe Tree

label :: MkTree
label = Label <$> do
  L x f <- get
  put f
  pure x

tree :: MkTree
tree = atree <|> label

atree :: MkTree
atree = do
  D f <- get
  put f
  t <- Tree <$> tree <*> tree
  U r <- get
  put r
  pure t

mkTree :: Euler -> Maybe Tree
mkTree = evalStateT atree

main :: IO ()
main = do
  (xs :: [Euler]) <-
    runResourceT $
      SBS.readFile "aoc18.txt"
        & SBS.lines
        & S.mapped SBS.toStrict
        & do S.map $ fmap mkEuler . parseOnly parseTree
        & S.concat
        & do S.toList_
  print . wtf $ List.foldl1 simplify  xs
  print . maximum . fmap (wtf . uncurry simplify) $ couples xs

run :: Euler -> Euler
run = safeLast $ fmap (fmap $ identity &&& identity) rules

safeLast :: (p -> Maybe (p, p)) -> p -> p
safeLast f x = case unfoldr f x of
  []  -> x
  xs -> unsafeLast xs
simplify :: Euler -> Euler -> Euler
simplify x y = run $ add x y

magnitude :: Tree -> Int
magnitude (Label x) = x
magnitude (Tree x y) = 3 * magnitude x + 2 * magnitude y



type TP = StateT String Maybe 

takeN :: Int -> TP String 
takeN n = StateT $ Just . splitAt n

parseR  :: Show a => a -> TP a
parseR x = do 
  (takeN (length sx) >>= guard  . (== sx))  $> x 
  where sx = show x 
  
testBackTracking :: Bool
testBackTracking = evalStateT (parseR 123 <|> parseR  124) "124" == Just (124 :: Int)