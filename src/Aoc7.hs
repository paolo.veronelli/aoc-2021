{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc7 where

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Protolude
import Protolude.Partial (read)
import Protolude.Unsafe (unsafeHead)

half :: Integral a => a -> a -> a
half x y = (x + y) `div` 2

sumdiff :: (Foldable f, Functor f, Num a, Num b) => (b -> a) -> f b -> b -> a
sumdiff f xs y = sum $ fmap (f . abs . subtract y) xs

sumsucc :: Integral a => a -> a
sumsucc x = x * (x + 1) `div` 2

bottom
  :: (Int -> Int)
  -> Int -- left
  -> Int -- right
  -> Int -- middle
  -> Int
bottom f p s x
  | fp >= fx && fs >= fx = minimum [fp, fx, fs]
  | fp > fx = bottom f x s $ half x (s + 1)
  | fs > fx = bottom f p x $ half p x
  | otherwise = panic "do some math or surrender!"
  where
    fp = f (x - 1)
    fx = f x
    fs = f (x + 1)

search :: (Functor t, Foldable t) => (Int -> Int) -> t Int -> Int
search f xs = bottom
  do sumdiff f xs
  do minxs
  do maxxs
  do half minxs maxxs
  where
    minxs = minimum xs
    maxxs = maximum xs

main :: IO ()
main = do
  xs <-
    fmap (read . T.unpack)
      . T.splitOn ","
      . unsafeHead
      . T.lines
      <$> T.readFile do "aoc7.txt"
  print $ search identity xs
  print $ search sumsucc xs
