{-# LANGUAGE TypeApplications #-}

module Aoc6'XE where

import Data.List (foldl')
import System.Environment (getArgs)

count :: (a -> Bool) -> [a] -> Int
count p = foldl' f 0
  where
    f n x | p x = n + 1
      | otherwise = n

getInput :: (String -> a) -> Int -> IO a
getInput parse _day =
  do
    args <- getArgs
    parse <$> case args of
      [] -> readFile "aoc6.txt"
      "-" : _ -> getContents
      fn : _ -> readFile fn

getInputLines :: (String -> a) -> Int -> IO [a]
getInputLines parse = getInput (map parse . lines)

main :: IO ()
main =
  do
    inp <- getInput parse 6
    print (part1 inp)
    print (part2 inp)
  where
    parse xs = read @[Int] $ '[' : xs ++ "]"

timers :: (Num a, Enum a, Eq a) => [a] -> [Integer]
timers xs = [fromIntegral (count (i ==) xs) | i <- [0 .. 8]] :: [Integer]

part1 :: (Num a, Enum a, Eq a) => [a] -> Integer
part1 fish = sum $ zipWith (*) (timers fish) [1421, 1401, 1191, 1154, 1034, 950, 905, 779, 768]

part2 :: (Num a, Enum a, Eq a) => [a] -> Integer
part2 fish =
  sum $
    zipWith
      (*)
      (timers fish)
      [ 6703087164
      , 6206821033
      , 5617089148
      , 5217223242
      , 4726100874
      , 4368232009
      , 3989468462
      , 3649885552
      , 3369186778
      ]
