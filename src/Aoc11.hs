{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Aoc11 where

import Control.Arrow ((>>>))
import Control.Lens (TraversableWithIndex (itraverse))
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import qualified Data.Text as T
import qualified Data.Text.IO as T
import Protolude
import qualified Protolude as Set
import Streaming (Of ((:>)))
import qualified Streaming.Prelude as S

main :: IO ()
main = do
  vs <-
    M.fromList
      . concatMap
        do \(l, xs) -> [((l, c), x) | (c, x) <- zip [1 ..] $ fmap digitToInt . T.unpack $ xs]
      . zip [1 ..]
      . T.lines
      <$> T.readFile do "aoc11.txt"
  let l@(lx, ly) = fst $ M.findMax vs
  print $
    runIdentity $
      do S.each $ iterate (step l . fst) (vs, mempty)
        & do
          \s -> do
            r1 :> rest <-
              S.splitAt 101 s
                & do S.map (length . snd)
                & do S.sum
            r2 <-
              rest
                & S.takeWhile do (< lx * ly) . length . snd
                & S.length_
            pure (r1, r2 + 101)

type Board = Map Point Int

add1 :: Board -> ([Point], Board)
add1 = itraverse @Point @(Map Point) \p (succ -> x) ->
  if x > 9 then ([p], 0) else ([], x)

type Point = (Int, Int)

neighbors :: Point -> Set Point -> Point -> [Point]
neighbors (lx, ly) s (x, y) = do
  x' <- [x - 1 .. x + 1]
  y' <- [y -1 .. y + 1]
  guard $
    (x /= x' || y /= y')
      && x' > 0
      && x' <= lx
      && y' > 0
      && y' <= ly
      && Set.notElem (x', y') s
  pure (x', y')

collectChange :: [Point] -> Board
collectChange = M.unionsWith (+) . fmap (`M.singleton` 1)

update :: Board -> (Point, Int) -> (Board, [Point])
update m (p, v) = second judge $ swap $ M.insertLookupWithKey insert p v m
  where
    judge :: Maybe Int -> [Point]
    judge Nothing = []
    judge (Just x)
      | x + v > 9 = [p]
      | otherwise = []
    insert :: Point -> Int -> Int -> Int
    insert _ delta ((+ delta) -> new)
      | new > 9 = 0
      | otherwise = new

explosion :: Point -> Set Point -> Board -> [Point] -> (Board, [Point])
explosion l s x =
  concatMap (neighbors l s)
    >>> collectChange
    >>> M.assocs
    >>> mapAccumL update x
    >>> second concat

exhaust :: Point -> Set Point -> [Point] -> Board -> (Board, Set Point)
exhaust l s ps m =
  case ps' of
        [] -> (m', s)
        _ -> exhaust l (s <> Set.fromList ps') ps' m'
  where (m', ps') = explosion l s m ps

step :: Point -> Map Point Int -> (Board, Set Point)
step l m = exhaust l s ps m'
  where
    (ps, m') = add1 m
    s = Set.fromList ps

