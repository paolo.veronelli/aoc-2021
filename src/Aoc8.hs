{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc8 where

import Control.Arrow ((>>>))
import qualified Data.List as L
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import Data.String (String)
import Protolude hiding (State)
import qualified Streaming.Prelude as S

parseLine :: [Char] -> ([String], [String])
parseLine l =
  let (cs, _ : gs) = splitAt 10 $ L.words l
   in (sort <$> cs, sort <$> gs)

main :: IO ()
main = do
  print <=< S.readFile "aoc8.txt" $ \l ->
    l & S.map parseLine
      & do
        S.map $ \(codes, guess) ->
          (\m -> fmap (m M.!) guess) . getMapping
            <$> pruningState codes
      & S.concat
      & do
        S.store $
          flip S.for S.each
            >>> do S.filter (`elem` [1, 4, 7, 8])
            >>> S.length_
      & do S.map $ foldl' (\s x -> x + 10 * s) 0
      & S.sum

data Trie = Trie (Maybe Int) (Map Char Trie) deriving (Show)

emptyTrie :: Trie
emptyTrie = Trie Nothing mempty

insert :: Int -> [Char] -> Trie -> Trie
insert n "" (Trie _ tr) = Trie (Just n) tr
insert n (x : xs) (Trie q t) =
  Trie q $ fromMaybe emptyTrie (M.lookup x t) & \t' -> M.insert x (insert n xs t') t

digitTrie :: Trie
digitTrie = foldl'
  do flip $ uncurry insert
  do emptyTrie
  do
    zip
      [0 ..]
      digits

digits :: [[Char]]
digits = ["abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg"]

data Check t = End t | Go t | Fail

-- permute all 'a'
pruning
  :: ((l, a) -> t -> Check t) -- state expansion
  -> t -- initial state
  -> [l] -- level context tracking
  -> [a] -- things to permute
  -> [t] -- good states
pruning f  s0 ks0 = go ks0 s0 <*> length
  where
    go _ _ _ 0 = []
    go (k : ks) s xs l = do
      (y : ys) <- fmap (take l) $ take l $ tails $ cycle xs
      case f (k, y) s of
        End s' -> pure s'
        Go s' -> go ks s' ys (l - 1)
        Fail -> []
    go _ _ _ _ = []

data Digit
  = Running
      String --  passed chars
      (Set Char) -- chars to go
      Trie -- chances to complete
  | Done (String, Int) -- winning  mapping
  | Abort -- no chances
  deriving (Show)

type State = [Digit]

addAssoc :: (Char, Char) -> Digit -> Digit
addAssoc (c, d) = \case
    Abort -> Abort
    Done x -> Done x
    r@(Running _ ((Set.member d -> False)) _) -> r
    Running acc xs (Trie _ tr) ->
      let acc' = d : acc
          xs' = Set.delete d xs
       in case M.lookup c tr of
            Nothing -> Abort
            Just h'@(Trie pot _) ->
              if Set.null xs'
                then maybe
                  Abort
                  do \gold -> Done (sort acc', gold)
                  do pot
                else Running acc' xs' h'

judgeState :: State -> Check State
judgeState s = go True s
  where
    go True [] = End s
    go _ [] = Go s
    go _ (Abort : _) = Fail
    go n (Done _ : xs) = go n xs
    go _ (_ : xs) = go False xs

pruningState :: [String] -> Maybe State
pruningState codes = head $ pruning  (fmap judgeState . fmap . addAssoc) (g <$> codes) "abcdefg" "abcdefg"
  where
    g x = Running "" (Set.fromList x) digitTrie

getMapping :: [Digit] -> Map String Int
getMapping = M.fromList . fmap \(Done x) -> x
