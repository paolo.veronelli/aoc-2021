{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

-- {-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc3 where

import Control.Foldl (Fold (..), purely)
import qualified Control.Foldl as F
import Protolude
import Streaming (Of (..), Stream)
import qualified Streaming.Prelude as S

peekHead :: (Monad m) => Stream (Of a) m b -> m (Either b (a, Stream (Of a) m b))
peekHead s = do
  S.next s <&> either
    do Left
    do \(x, rest) -> Right (x, S.yield x >> rest)

main :: IO ()
main = do
  (r1, r2) <- S.readFile "aoc3-big.txt" $ \s0 -> do
    ms <- peekHead s0
    case ms of
      Left _ -> panic "empty stream"
      Right (k, s) -> purely
        do S.fold_
        do (,) <$> cata1 (length k) <*> cata2
        do S.map (fmap boolC) s
  print $ solve1 r1
  print $ solve2 r2

boolC :: Char -> Bool
boolC = \case
  '0' -> False
  '1' -> True
  _ -> panic "NaB"

--------------------------- cata 1 ----------------------------

cata1 :: Int -> Fold [Bool] ([Int], Int)
cata1 k =
  (,)
    <$> do
      Fold
        do parseLine
        do replicate k 0
        do identity
    <*> do F.length

parseLine :: Enum c => [c] -> [Bool] -> [c]
parseLine = zipWith \n -> \case
  True -> succ n
  False -> n

solve1 :: ([Int], Int) -> Int
solve1 (vs, c) = flip F.fold vs $ Fold
  do \(due -> x, due -> y) v -> if v >= c `div` 2 then (succ x, y) else (x, succ y)
  do (0, 0)
  do uncurry (*)

due :: Num a => a -> a
due = (* 2)

----------------------- cata 2 ----------------------
solve2 :: T -> Int
solve2 r = number (high r) * number (low r)
  where
    number = foldl'
      do
        \(due -> c) -> \case
          False -> c
          True -> succ c
      do 0

cata2 :: Fold [Bool] T
cata2 = Fold
  do flip insert
  do tE
  do identity

data B = B Int T | EB deriving (Show)

data T = T B B deriving (Show)

tE :: T
tE = T EB EB

insert :: [Bool] -> T -> T
insert [] t = t
insert (False : xs) (T EB b1) = T (B 1 $ insert xs tE) b1
insert (False : xs) (T (B n0 t0) b1) = T (B (succ n0) $ insert xs t0) b1
insert (True : xs) (T b0 EB) = T b0 (B 1 $ insert xs tE)
insert (True : xs) (T b0 (B n1 t1)) = T b0 (B (succ n1) $ insert xs t1)

high :: T -> [Bool]
high (T EB EB) = []
high (T EB (B _ t1)) = True : high t1
high (T (B _ t0) EB) = False : high t0
high (T (B n0 t0) (B n1 t1))
  | n0 <= n1 = True : high t1
  | n0 > n1 = False : high t0
  | otherwise = panic "ghc nuts"

low :: T -> [Bool]
low (T EB EB) = []
low (T EB (B _ t1)) = True : low t1
low (T (B _ t0) EB) = False : low t0
low (T (B n0 t0) (B n1 t1))
  | n0 <= n1 = False : low t0
  | n0 > n1 = True : low t1
  | otherwise = panic "ghc nuts"
