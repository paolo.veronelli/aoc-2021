{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude, StrictData #-}
{-# LANGUAGE NoMonomorphismRestriction, BangPatterns #-}

module Aoc22 where

import Control.Lens
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString.Char8
import qualified Data.Set as Set
import Protolude
import Streaming
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S
import Control.Arrow ((&&&))

parseL :: Parser (Bool, [(Int, Int)])
parseL = do
  r <-
    do True <$ string "on "
      <|> do False <$ string "off "
  (r,) <$> sepBy
    do
      s <- anyChar >> char '=' >> signed decimal
      string ".." >> do (s,) <$> signed decimal
    do ","


data P = P 
  { px :: Int 
  , py :: Int
  , pz :: Int
  } deriving (Eq, Ord, Show)

mkQ :: (Bool, [(Int, Int)]) -> Q
mkQ (r, [(xm, xM), (ym, yM), (zm, zM)]) = Q (if r then Pos else Neg) (C (P xm ym zm) (P xM yM zM))
mkQ _ = panic "no Q"

main :: IO ()
main = do
  print <=< runResourceT $
    SBS.readFile "aoc22.txt"
      & SBS.lines
      & S.mapped SBS.toStrict
      & S.map do parseOnly parseL
      & S.concat
      & S.map mkQ
      & S.store 
          do \x -> x 
                & S.map do \(Q r c) -> Q r <$> intersect (C (P (-50) (-50) (-50)) (P 50 50 50)) c
                & S.concat 
                & do folder 
      & folder 
      
folder :: Monad m => Stream (Of Q) m r -> m (Of Int r)
folder = S.fold
        do evolve 
        do []
        do sum . fmap size

data C = C P P deriving Show

intersect :: C -> C -> Maybe C
intersect p1 p2 = do
  (xm, xM) <- intersect1D px p1 p2
  (ym, yM) <- intersect1D py p1 p2
  (zm, zM) <- intersect1D pz p1 p2
  pure $ C (P xm  ym zm) (P xM yM zM)
  where
    intersect1D l (C (l -> am) (l -> aM)) (C (l -> bm) (l -> bM))
      | bm > aM || am > bM = Nothing
      | otherwise = Just (max am bm, min aM bM)

data S = Pos | Neg deriving Show

data Q = Q S C deriving Show

type Pop = [Q]

addQ :: Q -> Q -> [Q]
addQ (Q Pos c1) (Q Pos c2) = case c1 `intersect` c2 of
  Nothing -> []
  Just cd -> [Q Neg cd]
addQ (Q Pos c1) (Q Neg c2) =  case c1 `intersect` c2 of
  Nothing -> []
  Just cd -> [Q Pos cd]
addQ (Q Neg c1) (Q Neg c2) = case c1 `intersect` c2 of
  Nothing -> []
  Just cd -> [Q Pos cd]
addQ (Q Neg c1) (Q Pos c2) = case c1 `intersect` c2 of
  Nothing -> []
  Just cd -> [Q Neg cd]

size :: Q -> Int
size (Q s (C (P x y z) (P x' y' z'))) = r $ f (x - x') * f (y - y') * f (z - z')
  where
    f v = abs v + 1
    r = case s of
      Pos -> identity
      Neg -> negate


evolve :: Pop -> Q -> Pop
evolve [] q@(Q Pos _) = [q]
evolve [] _ = []
evolve !qs x@(Q s _) = (qs <>)  . r  $ qs >>= addQ x 
  where 
    r = case s of 
      Pos -> (x:)
      Neg -> identity

