{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Aoc13 where

import qualified Control.Foldl as FL
import Control.Monad.State.Strict
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString.Char8 (Parser, decimal, parseOnly, string)
import qualified Data.Set as Set
import Protolude hiding (StateT, evalState, evalStateT, runStateT)
import Protolude.Unsafe (unsafeIndex, unsafeLast)
import Streaming (Of ((:>)))
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S
import System.IO (putChar)

parseX :: Monad m => Parser a -> S.Stream (Of ByteString) m r -> S.Stream (Of a) m r
parseX l = S.concat . S.map (parseOnly l)

type P = (Int, Int) -- point
type L = Either Int Int -- vert or hor line of fold

parsePoint :: Parser P
parsePoint = do
  x <- decimal
  void $ string ","
  (x,) <$> decimal

parseFold :: Parser L 
parseFold = do
  void $ string "fold along "
  Left <$> (string "x=" >> decimal)
    <|> Right <$> (string "y=" >> decimal)

foldXY :: L -> P -> P
foldXY (Right l) p@(x, y)
  | y > l = (x, 2 * l - y)
  | otherwise = p
foldXY (Left l) p@(x, y)
  | x > l = (2 * l - x, y)
  | otherwise = p

main :: IO ()
main = do
  (points, folds) <-
    runResourceT $
      SBS.readFile "aoc13.txt"
        & SBS.lines
        & S.mapped SBS.toStrict
        & do
          \s -> do
            ps :> sfs <-
              s
                & S.break (== mempty)
                & parseX parsePoint
                & FL.purely S.fold FL.set
            fs <- S.toList_ $ parseX parseFold sfs
            pure (ps, fs)
  let qs = scanl'
        do \ps f -> Set.map (foldXY f) ps
        do points
        do folds
      lqs = unsafeLast qs
      (xm, ym) = Set.findMin lqs
      (xM, yM) = Set.findMax lqs
  print $ length $ unsafeIndex qs 1
  forM_ [ym .. yM] $ \y -> do
    forM_ [xm .. xM] $ \x ->
      putChar $ if Set.member (x, y) lqs then '#' else ' '
    putText ""
