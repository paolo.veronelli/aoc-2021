{-# LANGUAGE BlockArguments #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}
{-# LANGUAGE BangPatterns #-}

module Aoc14'Alecs where

import Control.Arrow (Arrow ((&&&)))
import Data.List (foldl')
import Data.List.Split (splitOn)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

main :: IO ()
main = do
  fileContent <- readFile "aoc14.txt"
  let (polymer, rules) = parse fileContent
  print $ solve 10  rules polymer
  print $ solve 40  rules polymer

type P = (Char, Char)

type Compress = Map P Integer

type Rules = Map P Char

parse :: [Char] -> (Compress, Rules)
parse input =
  let [polymer, rules] = splitOn "\n\n" input
   in ( compress polymer
      , M.fromList $ parseRule . words <$> lines rules
      )
  where
    parseRule [[fst, snd], _, [to]] = ((fst, snd), to)
    compress = foldl' insert M.empty . uncurry zip . (id &&& drop 1) . (' ' :)
    insert m e = M.insertWith (+) e 1 m

apply :: Rules -> Compress -> Compress
apply rules = M.fromListWith (+) . concatMap step . M.assocs
  where
    step :: (P, Integer) -> [(P, Integer)]
    step t@(pair@(l, r), count) = maybe
      do [t]
      do
        \mid ->
          [ ((l, mid), count)
          , ((mid, r), count)
          ]
      do rules M.!? pair

solve :: Int -> Rules -> Compress -> Integer
solve n rules polymer =
  uncurry (-)
    . (maximum &&& minimum)
    . M.mapKeysWith (+) snd
    $ iterate (apply rules) polymer !! n
