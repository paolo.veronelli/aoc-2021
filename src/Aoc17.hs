{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Aoc17 where

import qualified Data.Map.Monoidal.Strict as M
import qualified Data.Set as Set
import Protolude

xm :: Int
xm = 137

xM :: Int
xM = 171

ym :: Int
ym = -73

yM :: Int
yM = -98


increment :: Int -> [Int]
increment x = scanl (+) 0 
  do takeWhile (> 0) (iterate (subtract 1) x) <> repeat 0

xmatches :: [(Int, Int)]
xmatches = do
  vx <- [floor $ -1 + sqrt (1 + 8 * fromIntegral xm) / 2 .. xM]
  (t, q) <- zip [0 .. xM + ym - yM] $ increment vx
  guard $ q >= xm
  guard $ q <= xM
  pure (t, vx)

tOfy :: Int -> Int -> Double
tOfy v y =
  let b = fromIntegral $ 2 * v + 1
   in (b + sqrt (b ^ 2 - 8 * fromIntegral y)) / 2

yOft :: Int -> Int -> Int
yOft v t = t * (2 * v + 1 - t) `div` 2

ymatches :: [(Int, Int)]
ymatches = do
  v <- [yM .. - yM]
  t <- [floor $ tOfy v ym .. ceiling $ tOfy v yM]
  let y = yOft v t
  guard $ y <= ym && y >= yM
  pure (t, v)

collect :: Ord a => [(Int, a)] -> M.MonoidalMap Int [a]
collect = mconcat . fmap (\(k, x) -> M.singleton k [x])

main :: MonadIO m => m ()
main = do
  print $ yM * (yM + 1) `div` 2
  print $
    length $
      Set.fromList $
        fold $ M.intersectionWith
          do \xs ys -> (,) <$> toList xs <*> toList ys
          do collect xmatches
          do collect ymatches
