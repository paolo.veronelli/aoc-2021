{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc1_2 where

import Control.Monad.Trans.Resource (runResourceT)
import qualified Data.Attoparsec.ByteString.Char8 as A
import Data.Sequence (Seq ((:<|), (:|>)))
import Data.String (String)
import Protolude
import Streaming (Of (..), Stream)
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

main :: IO ()
main = runResourceT do
  p3 :> p1 :> () <-
    SBS.readFile "aoc1.txt"
      & SBS.lines
      & S.mapped readInt
      & flip
        S.for
        do either (const $ pure ()) S.yield
      & S.copy
      & program 1
      & program 3
  print p1
  print p3

readInt :: Monad m => SBS.ByteStream m x -> m (Of (Either String Int) x)
readInt = fmap (first $ A.parseOnly A.decimal) . SBS.toStrict

program :: Monad m => Int -> Stream (Of Int) m r -> m (Of Int r)
program n s =
  s
    & S.slidingWindow (n + 1)
    & S.map
      do \t -> qhead t < qtail t
    & S.filter identity
    & S.length
  where
    qtail (_ :|> x) = x
    qhead (x :<| _) = x
