{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc16 where

import Control.Arrow ((&&&))
import Control.Monad.Trans.Resource (runResourceT)
import Protolude hiding (try, many)
import Protolude.Unsafe (unsafeRead)
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S
import StreamingParsec (withParsec)
import Text.Parsec
  ( ParsecT
  , anyToken
  , count
  , lookAhead
  , many, try
  )
import qualified Text.Parsec as P 

h :: Char -> [Char]
h = \case
  '0' -> "0000"
  '1' -> "0001"
  '2' -> "0010"
  '3' -> "0011"
  '4' -> "0100"
  '5' -> "0101"
  '6' -> "0110"
  '7' -> "0111"
  '8' -> "1000"
  '9' -> "1001"
  'A' -> "1010"
  'B' -> "1011"
  'C' -> "1100"
  'D' -> "1101"
  'E' -> "1110"
  'F' -> "1111"

type E = (Int, Char)

number :: (Foldable t, Num a, Read a) => t Char -> a
number = foldl'
  do \s x -> s * 2 + unsafeRead (pure x)
  do 0

numberP :: (Num b, Read b, P.Stream s m (a, Char), Show a) => Int -> ParsecT s u m b
numberP r = number . fmap snd <$> boolsP r

data Packet = Packet Int Int | Op Int Int [Packet] deriving (Show)

boolsP :: (P.Stream s m a, Show a) => Int -> ParsecT s u m [a]
boolsP r = replicateM r anyToken

parsePacket :: (P.Stream s m (t, Char), Eq t, Show t, Num t, Read t, MonadIO m) => ParsecT s u m Packet
parsePacket = do
  v <- numberP 3
  i <- numberP 3
  case i of
    4 -> do
      rs <- many $ try do
        '1' <- snd <$> anyToken
        boolsP 4
      '0' <- snd <$> anyToken
      es <- boolsP 4
      pure $ Packet v $ number $ fmap snd $ concat $ rs <> [es]
    _ ->
      Op v i
        <$> do
          l <- snd <$> anyToken
          q <-
            if l == '1'
              then Right <$> numberP 11
              else Left <$> numberP 15
          case q of
            Left n -> do
              parseUpTo n parsePacket
            Right n -> do
              count n parsePacket

parseUpTo :: (P.Stream s m (t, b), Num t, Eq t, Show t, Show b) => t -> ParsecT s u m a -> ParsecT s u m [a]
parseUpTo 0 _p = pure []
parseUpTo n p = do
  s <- fst <$> lookAhead anyToken
  x <- p
  e <- fst <$> lookAhead anyToken
  (x :) <$> parseUpTo (n - (e - s)) p

char :: (P.Stream s m (a1, a2), Show a1, Show a2, Eq a2) => a2 -> ParsecT s u m ()
char x = do
  y <- snd <$> anyToken
  guard $ x == y

main :: IO ()
main = do
  err <-
    runResourceT $
      SBS.readFile "aoc16.txt"
        & SBS.unpack
        & S.filter isAlphaNum
        & flip S.for (S.each . h)
        & S.zip (S.each [0 :: Int ..])
        & withParsec parsePacket
        & S.map do versions &&& value
        & S.print
  print err

-- & do S.mapped $ fmap (first $ concatMap h . B.unpack) . SBS.toStrict
-- & do S.map $ zip [0 ..]
-- & do S.mapM $
-- & S.concat
-- & S.print

value :: Packet -> Int
value (Packet _ x) = x
value (Op _ 0 ps) = sum $ value <$> ps
value (Op _ 1 ps) = product $ value <$> ps
value (Op _ 2 ps) = minimum $ value <$> ps
value (Op _ 3 ps) = maximum $ value <$> ps
value (Op _ 5 [x, y]) = fromEnum $ value x > value y
value (Op _ 6 [x, y]) = fromEnum $ value x < value y
value (Op _ 7 [x, y]) = fromEnum $ value x == value y

versions :: Packet -> Int
versions (Packet v _) = v
versions (Op v _ ps) = v + sum (fmap versions ps)
