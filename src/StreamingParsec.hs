{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleContexts #-}

module StreamingParsec where

import Protolude
import Streaming
import Streaming.ByteString.Char8 (ByteStream)
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S
import Text.Parsec (Consumed (Consumed, Empty), ParsecT, Reply (Ok, Error), runParsecT, ParseError)
import qualified Text.Parsec as P
import Text.Parsec.Pos (initialPos)

newtype StreamChar m = StreamChar (ByteStream m ())

instance Monad m => P.Stream (StreamChar m) m Char where
  uncons (StreamChar s) = do
    r <- SBS.uncons s
    case r of
      Left () -> pure Nothing
      Right (b, s') -> pure $ Just (b, StreamChar s')

newtype StreamOfA m a = StreamOfA {getStreamOfA :: Stream (Of a) m ()}

instance Monad m => P.Stream (StreamOfA m a) m a where
  uncons (StreamOfA s) = do
    r <- S.next s
    case r of
      Left () -> pure Nothing
      Right (b, s') -> pure $ Just (b, StreamOfA s')

withParsec :: (Monad m, MonadFail m) => (forall s. P.Stream s m a => ParsecT s () m b) -> Stream (Of a) m () -> Stream (Of b) m (Maybe ParseError )
withParsec p stream = effect $ do
  let go s = do
        ce  <- runParsecT p s
        case ce of 
          Consumed mr -> do 
              reply <- mr 
              case reply of 
                Ok x s' _pe -> pure $ S.yield x >> effect (go s')
                Error e -> pure $ pure (Just e)
          Empty _ -> pure $ pure Nothing
  go $ P.State (StreamOfA stream) (initialPos "what") ()
