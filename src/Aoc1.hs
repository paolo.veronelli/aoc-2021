{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc1 where

import Data.Sequence (Seq ((:<|), (:|>)))
import Protolude

import Streaming (Of (..), Stream)
import qualified Streaming.Prelude as S


main :: IO ()
main = do
  p3 :> p1 :> () <-
    S.readLn @_ @Int
      & S.copy
      & program 1
      & program 3
  print p1
  print p3

program :: Monad m => Int -> Stream (Of Int) m r -> m (Of Int r)
program n x =
  x
    & S.slidingWindow (n + 1)
    & S.map
      do \s -> qhead s < qtail s
    & S.filter identity
    & S.length
  where 
    qtail (_ :|> t) = t
    qhead (h :<| _) = h

