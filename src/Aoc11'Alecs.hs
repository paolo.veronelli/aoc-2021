{-# LANGUAGE TupleSections #-}

module Aoc11'Alecs where

import Control.Arrow ((&&&))
import Data.Bifunctor ( Bifunctor(second, first) )
import Data.Char ( digitToInt )
import Data.List ( foldl', findIndex )
import qualified Data.Map as M
import Data.Maybe ( fromJust )

main :: IO ()
main = do
  fileContent <- readFile "aoc11.txt"
  let input = M.fromList . parse $ lines fileContent
  print $ one 100 input
  print $ two input

one :: Int -> M.Map (Integer, Integer) Int -> Int
one steps = length . filter (== 0) . concat . take (steps + 1) . solve

two :: M.Map (Integer, Integer) Int -> Int
two = findJust (all (== 0)) . solve
  where
    findJust = fromJust .: findIndex
    (.:) = (.) . (.)

solve :: M.Map (Integer, Integer) Int -> [[Int]]
solve = fmap M.elems . iterate step

parse :: [[Char]] -> [((Integer, Integer), Int)]
parse = fmap (second digitToInt) . compact . coord . fmap coord
  where
    compact = foldl' (\a (i, x) -> a ++ fmap (first (i,)) x) []
    coord = zip [0 ..]

step :: M.Map (Integer, Integer) Int -> M.Map (Integer, Integer) Int
step = uncurry (foldl' flash) . (fmap (+ 1) &&& goingtoflash)
  where
    goingtoflash = fmap fst . filter ((== 9) . snd) . M.toList

flash :: (Ord a1, Ord b, Ord a2, Num a2, Num a1, Num b) => M.Map (a1, b) a2 -> (a1, b) -> M.Map (a1, b) a2
flash m o = case M.lookup o m of
  Just x
    | x >= 9 -> foldl' flash (M.insert o 0 m) (adj o)
    | x >= 1 -> M.insert o (x + 1) m
  _ -> m

adj :: (Num a, Num b) => (a, b) -> [(a, b)]
adj p =
  fmap
    (add p)
    [ (-1, 1)
    , (0, 1)
    , (1, 1)
    , (-1, 0)
    , (1, 0)
    , (-1, -1)
    , (0, -1)
    , (1, -1)
    ]
  where
    add (a, b) (c, d) = (a + c, b + d)
