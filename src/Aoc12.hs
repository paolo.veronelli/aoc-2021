{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-type-defaults #-}

module Aoc12 where

import Control.Arrow ((***))
import Control.Lens (Lens', (.~), use, (%=))
import qualified Control.Monad.Logic as L
import Control.Monad.State.Strict
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString.Char8
  ( Parser
  , parseOnly
  , string
  , takeByteString
  , takeTill
  )
import qualified Data.ByteString.Char8 as B
import Data.Map.Monoidal.Strict (MonoidalMap)
import qualified Data.Map.Monoidal.Strict as MM
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import Data.String (String)
import Protolude hiding (StateT, evalState, evalStateT, runStateT)
import Streaming (Of ((:>)))
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

-- node names to indices
type Mapping = (Map ByteString Int, Map Int ByteString)

-- graph as mapping from node to one time pass nodes * any time pass nodes
type Graph = MonoidalMap Int (Set Int, [Int])

parseLink
  :: Monad m
  => ByteString
  -> StateT Mapping m (Either String Graph)
parseLink l = do
  -- coouldn't figure out how to reuse the StateT, prob some monad base thing
  m <- get
  let r = flip parseOnly l $ flip runStateT m do
        x <- useIndex =<< lift (takeTill (== '-'))
        void $ lift $ string "-"
        y <- useIndex =<< lift takeByteString
        pure $ MM.fromList [(snd x, place y), (snd y, place x)]
  case r of
    Left s -> pure $ Left s
    Right (d, m') -> put m' $> Right d

-- create or retrieve a mapping  for the node name
useIndex :: ByteString -> StateT Mapping Parser (ByteString, Int)
useIndex b = do
  (bi, ib) <- get
  case M.lookup b bi of
    Just i -> pure (b, i)
    Nothing -> do
      let (succ -> k, _) = M.findMax ib
      modify (M.insert b k *** M.insert k b)
      pure (b, k)

place :: (ByteString, Int) -> (Set Int, [Int])
place ("start", _) = mempty
place (isUpper . B.head -> True, x) = (mempty, [x])
place (_, x) = (Set.singleton x, mempty)

-- tracks the already used nodes
data KState = KState Bool (Set Int) deriving (Eq, Show, Ord)

jollyL :: Lens' KState Bool
jollyL f (KState j s) = (`KState` s) <$> f j

-- inner monad free from backtracking to accumulate cache
newtype DynState = DynState {dynStateMap :: Map (Int, KState) Int}

dynStateL :: Lens' DynState (Map (Int, KState) Int)
dynStateL f (DynState x) = DynState <$> f x

dig :: Int -> Graph -> Bool -> Int
dig e m f = sum $
  flip evalState (DynState mempty) $
    L.observeAllT $
      ($ 0) $
        ($ KState f mempty) $ fix \go done -> \case
          ((==) e -> True) -> pure 1
          k -> do
            let (small, big) = m MM.! k
            (h, done') <-
              msum $
                pure
                  <$> do possibles done small <> fmap (addNode done) big
            dp <- use dynStateL
            case M.lookup (h, done') dp of
              Nothing -> do
                r <- fmap sum $ lift $ L.observeAllT $ go done' h
                dynStateL %= M.insert (h, done') r
                pure r
              Just r -> pure r

addNode :: KState -> Int -> (Int, KState)
addNode (KState j s) x = (x, KState j $ Set.insert x s)

possibles :: KState -> Set Int -> [(Int, KState)]
possibles q@(KState j s) xs =
  [addNode q x | x <- toList $ Set.difference xs s]
    <> if j
      then []
      else [addNode (q & jollyL .~ True) x | x <- toList $ Set.intersection s xs]

main :: IO ()
main = do
  (m, (M.! "end") . fst -> qe) :: (Graph, Mapping) <-
    runResourceT $
      flip runStateT (M.singleton "start" 0, M.singleton 0 "start") $
        SBS.readFile "aoc12.txt"
          & SBS.lines
          & S.mapped
            do
              \s -> do
                r :> rest <- SBS.toStrict s
                l <- parseLink r
                pure $ l :> rest
          & S.concat
          & S.fold_ (<>) mempty identity
  print $ dig qe m True
  print $ dig qe m False
