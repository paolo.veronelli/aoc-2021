{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-incomplete-patterns #-}

module Aoc2 where

import Control.Foldl (Fold (..), purely)
import Control.Monad.Trans.Resource (runResourceT)
import Data.Attoparsec.ByteString (Parser, parseOnly)
import Data.Attoparsec.ByteString.Char8 (decimal, space, string)
import Protolude
import Streaming (Of (..), Stream)
import qualified Streaming.ByteString.Char8 as SBS
import qualified Streaming.Prelude as S

readLine :: Monad m => Parser a -> Stream (SBS.ByteStream m) m r -> Stream (Of a) m r
readLine f x =
  x
    & S.mapped
      do fmap (first $ parseOnly f) . SBS.toStrict
    & do
      flip
        S.for
        do either (const $ pure ()) S.yield

type Command = Either Int Int

parseInstruction :: ByteString -> Parser Int
parseInstruction x = do
  string x
  space
  decimal

parseCommand :: Parser Command
parseCommand =
  Right <$> parseInstruction "forward"
    <|> Left <$> parseInstruction "down"
    <|> Left . negate <$> parseInstruction "up"

-- cannot use tuples as state, they are lazy
data Point = Point Int Int

track1 :: Fold Command Int
track1 = Fold
  do
    \(Point x y) -> \case
      Left v -> Point x (y + v)
      Right h -> Point (x + h) y
  do Point 0 0
  do \(Point x y) -> x * y

data Aimed = Aimed Int Point

track2 :: Fold Command Int
track2 = Fold
  do
    \(Aimed a p@(Point x y)) -> \case
      Left v -> Aimed (a + v) p
      Right h -> Aimed a $ Point (x + h) (y + a * h)
  do Aimed 0 $ Point 0 0
  do \(Aimed _ (Point x y)) -> x * y

main :: IO ()
main = runResourceT do
  (p1, p2) :> () <-
    SBS.readFile "aoc2.txt"
      & SBS.lines
      & readLine parseCommand
      & do purely S.fold $ (,) <$> track1 <*> track2
  print p1
  print p2
