{-# LANGUAGE NumericUnderscores #-}
module RandomSequence where

import System.Random
import qualified Streaming.Prelude as S
import Protolude
import Prelude hiding (show)

randomNumber :: (Random a, MonadIO m) => a -> a -> Int -> S.Stream (S.Of a) m ()
randomNumber b t n = do
  xs <- randomRs (b,t) <$> newStdGen  
  S.take n $ S.each xs

writeAoc1 :: IO ()
writeAoc1 = S.writeFile "aoc1-big.txt" 
  $ do S.map show 
  $ S.scan (+) (0 :: Int)  identity 
  $ do randomNumber (-5) 10 10000000


writeAoc3 :: IO ()
writeAoc3 = S.writeFile "aoc3-big.txt" 
  $ S.map toS 
  $ S.take 1_000_000
  $ do S.repeatM $ randomOneZero  12


randomOneZero :: MonadIO m => Int -> m [Char]
randomOneZero n = replicateM n $ randomRIO ('0', '1')