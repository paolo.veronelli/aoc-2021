{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Aoc19 where

import Control.Arrow ((&&&))
import Control.Lens
  ( At (at)
  , Field1 (_1)
  , Field2 (_2)
  , Field3 (_3)
  , use
  , view
  , (%=)
  , (.=)
  )
import Control.Monad.Logic
  ( LogicT
  , MonadLogic (once)
  , observeAllT, interleave
  )
import qualified Data.List.Split as List
import qualified Data.Map.Strict as M
import qualified Data.Set as Set
import Protolude
import Protolude.Unsafe (unsafeRead)
import qualified Streaming.Prelude as S
import Text.Pretty.Simple (pPrint)
import Protolude.Partial (foldr1)

type P = (Int, Int, Int)

neg :: P -> P
neg (x, y, z) = (- x, - y, - z)

sub, add :: P -> P -> P
sub p = add p . neg
add (x, y, z) (x', y', z') = (x + x', y + y', z + z')

orient :: P -> [P]
orient (x, y, z) =
  [ (x, y, z) :: P
  , (y, - x, z)
  , (y, z, x)
  , (- y, x, z)
  , (- x, - y, z)
  , (y, - z, - x)
  ]

spin :: P -> [P]
spin (x, y, z) =
  [ (x, y, z)
  , (x, - y, - z)
  , (x, - z, y)
  , (x, z, - y)
  ]

maxdist :: [P] -> Int
maxdist xs = maximum $ do
  uncurry manhattan <$>  combinations xs

manhattan (x, y, z) (x', y', z') = abs (x - x') + abs (y - y') + abs (z - z')
orientations :: P -> [P]
orientations = spin >=> orient

combinations :: [t] -> [(t, t)]
combinations [] = []
combinations (x : xs) = ((x,) <$> xs) <> combinations xs



pick :: [a] -> LogicT m a
pick = msum . fmap pure

main :: IO ()
main = do
  m:ms <- S.readFile "aoc19.txt" $ \l ->
    l & S.groupBy ((==) `on` null)
      & S.mapped do S.toList . S.drop 1
      & S.filter do not . null
      & S.map do fmap $ (\[x, y, z] -> (x, y, z)) . fmap unsafeRead . List.splitOn ","
      & S.zip do S.each [0 ..]
      & S.toList_ @_ @(Int, [P])
  r <- flip
    execStateT
    ( M.fromList (pure $ ((0, 0, 0),) <$> m)
    , [0]
    , M.fromList ms
    )
    . observeAllT
    $ fix \go -> do
      is <- use _2
      case is of
        [] -> pure ()
        (i : js) -> do
          _2 .= js
          Just (_, im) <- use $ _1 . at i
          (j, jm) <- use _3 >>= pick . M.assocs
          once $ do
            jc <- pick $ transpose $ orientations <$> jm
            (t, jm') <- match im jc
            _1 %= M.insert j (t, jm')
            _2 %= (j :)
            _3 %= M.delete j
          go
  pPrint
    . (length
         . Set.fromList
         . foldMap snd
         &&& maxdist . M.elems . fmap fst)
    $ view _1 r

match :: MonadIO m => [P] -> [P] -> LogicT m (P, [P])
match ps qs = do
  let s = Set.fromList qs
  p <- pick ps
  q <- pick  qs
  let t = sub q p
  guard $ (> 11) . length $ filter (`Set.member` s) $ add t <$>  ps
  pure (t, (`sub` t) <$> qs)
