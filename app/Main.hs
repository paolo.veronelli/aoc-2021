{-# OPTIONS_GHC -Wno-unused-imports #-}

import qualified Aoc1
import qualified Aoc10
import qualified Aoc11
import qualified Aoc11'Alecs
import qualified Aoc12
import qualified Aoc13
import qualified Aoc14
import qualified Aoc14'Alecs
import qualified Aoc1_2
import qualified Aoc2
import qualified Aoc3
import qualified Aoc5
import qualified Aoc6'GM
import qualified Aoc6'XE
import qualified Aoc7
import qualified Aoc8
import qualified Aoc9
import qualified Aoc9'2
import qualified Aoc9'3
import qualified Aoc9'Alecs
import qualified Aoc9'FA
import qualified Aoc15
import qualified Aoc16
import qualified Aoc19
import qualified Aoc18
import qualified Aoc22
import Protolude

main :: IO ()
main = void  Aoc22.main
